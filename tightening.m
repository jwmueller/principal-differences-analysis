function [beta, wass_dist] = tightening(xs, ys, max_iter, eps, learning_rate, print_update, beta0)
    % Greedy hill-climbing method which enforces fixed cardinality of beta
    % found in semidefinite relaxation step.
    [n,d] = size(xs); m = size(ys,1);
    iter = 0;
    if ~exist('beta0', 'var')
        beta0 = repmat(sqrt(d)/d,d,1) / 2;
    end
    beta = beta0;
    beta_cardinality = sum(abs(beta0) > 0); % the allowed number of nonzero entries in this tightening stage.
    last_cost = -Inf;
    cost = projectedWasserstein(xs, ys, beta);
    grad = zeros(d,1);
    while (iter < max_iter) && (cost - last_cost >= eps)
        last_cost = cost; last_beta = beta;
        iter = iter + 1;
        step_size = learning_rate / sqrt(iter);
        if any([0] == mod(iter, print_update))
            disp(['iter: ' num2str(iter)  '   cost: ' num2str(cost)  '  grad-norm: ' num2str(norm(grad)) '    beta_norm: ' num2str(norm(beta)) '  Step-size: ' num2str(step_size)])
        end
        % get gradient:
        [projected_xs, x_order] = sort(xs * beta);
        [projected_ys, y_order] = sort(ys * beta);
        grad = zeros(d,1);
        quant_x = 0;
        quant_y = 0;
        last_quant = 0;
        NUMERIC_FACTOR = 1e-6;
        delta = NUMERIC_FACTOR/(n*m); % small numerical factor used to reliably convert floats to integer indices.
        x_index = 1; y_index = 1;
        while (quant_x < 1 - delta) || (quant_y < 1 - delta)
           next_quant_x = quant_x + 1/n;
           next_quant_y = quant_y + 1/m;
           proj_x = projected_xs(x_index);
           proj_y = projected_ys(y_index);
           while (next_quant_x < next_quant_y - delta)
               grad = grad + 2 *(proj_x - proj_y) * (xs(x_order(x_index),:) - ys(y_order(y_index),:))' * (next_quant_x-last_quant);
               quant_x = next_quant_x;
               last_quant = quant_x;
               next_quant_x = quant_x + 1/n;
               x_index = min(x_index + 1, n);
               proj_x = projected_xs(x_index);
           end
           if (quant_x < 1 - delta) || (quant_y < 1 - delta)
               if (abs(next_quant_x - next_quant_y) < delta)
                   grad = grad + 2 *(proj_x - proj_y) * (xs(x_order(x_index),:) - ys(y_order(y_index),:))' * (next_quant_x-last_quant);
                   quant_x = next_quant_x;
                   quant_y = next_quant_y;
                   x_index = x_index + 1; y_index = y_index + 1;
                   last_quant = quant_x;
               else
                   grad = grad + 2 *(proj_x - proj_y) * (xs(x_order(x_index),:) - ys(y_order(y_index),:))' * (next_quant_x-last_quant);
                   quant_y = next_quant_y;
                   y_index = min(y_index + 1, m);
                   last_quant = quant_y;
               end
           end
        end % End of gradient computation.

        MAX_BACTRACKING = 10; % number of points to consider in backtracking line search for step-size.
        backtrack_tries = 0;
        while (cost - last_cost < eps) && (backtrack_tries <= MAX_BACTRACKING)
            step_size = learning_rate / log(iter+1) * 2^(-backtrack_tries);
            beta = beta + step_size * grad;
            if beta(1) < 0
                beta = -beta;
            end
            
            % truncation step (i.e. at most k nonzero entries, hard-contraint).
            [~,ordering] = sort(abs(beta),'descend');
            beta(ordering((beta_cardinality+1):end)) = 0; % truncate.
            
            beta_norm = norm(beta);
            if beta_norm > 1
                beta = beta / norm(beta);
            end
            cost = projectedWasserstein(xs, ys, beta);
            backtrack_tries = backtrack_tries + 1;
        end
        if cost < last_cost % no improvement.
            cost = last_cost;
            beta = last_beta;
            break
        end
    end
    wass_dist = cost;
    if iter >= max_iter
        warning('Tightening optimization failed to converge (likely max_iter or learning_rate is too small)')
    end
end


