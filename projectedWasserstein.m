function [ dist ] = projectedWasserstein(X_samples, Y_samples, beta)
   % Projected SQUARED empirical Wasserstein distance in direction beta
   % between X_samples and Y_samples.

   NUMERIC_FACTOR = 1e6; % used in ceil function to get the index of the quantile in the projected data vector.
   
   projected_xs = sort(X_samples * beta);
   projected_ys = sort(Y_samples * beta);
   if length(projected_xs) < length(projected_ys)
       temp = projected_xs;
       projected_xs = projected_ys;
       projected_ys = temp;
   end
   n = length(projected_xs);
   m = length(projected_ys);
   eps = 1/(n*m*NUMERIC_FACTOR);
   dist = 0;
   quant_x = 0;
   quant_y = 0;
   last_quant = 0;
   x_index = 1; y_index = 1;
   while (quant_x < 1 - eps) || (quant_y < 1 - eps)
       next_quant_x = quant_x + 1/n;
       next_quant_y = quant_y + 1/m;
       proj_x = projected_xs(x_index);
       proj_y = projected_ys(y_index);
       while (next_quant_x < next_quant_y - eps)
           dist = dist + (proj_x - proj_y)^2 * (next_quant_x-last_quant);
           quant_x = next_quant_x;
           last_quant = quant_x;
           next_quant_x = quant_x + 1/n;
           x_index = min(x_index + 1, n);
           proj_x = projected_xs(x_index);
       end
       if (quant_x < 1 - eps) || (quant_y < 1 - eps)
           if (abs(next_quant_x - next_quant_y) < eps)
               dist = dist + (proj_x - proj_y)^2 *(next_quant_x-last_quant);
               quant_x = next_quant_x;
               quant_y = next_quant_y;
               x_index = x_index + 1; y_index = y_index + 1;
               last_quant = quant_x;
           else
               dist = dist + (proj_x - proj_y)^2 *(next_quant_y-last_quant);
               quant_y = next_quant_y;
               y_index = min(y_index + 1, m);
               last_quant = quant_y;
           end
      end
   end
end