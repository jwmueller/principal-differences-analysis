function [] = plotProjectedDistributions( X_samples, Y_samples, beta, varargin)
% Plots kernel density estimates of the projected X,Y distributions 
% for a given direction beta.
% Optional arguments:
% 'bandwidth_sizes' controls the bandwidth setting used in per-class estimates 
% (should be a vector of length 2).  If unspecified, optimal bandwidth
% assuming Gaussian distributions is employed.
% 'class_plot_style' = vector of two strings specifying line-type/color to use for each class.
% 'show_legend' = true/false
% 'class_names' = vector of two strings specifying names of each class in legend.
% 'x_axis' = string specifying name of x-axis.
% 'y_axis' = string specifying name of y-axis.
    p = inputParser; p.FunctionName = 'plotProjectedDistributions'; % Parse input arguments.
    bandwidth_sizes_default = [0 0]; addOptional(p, 'bandwidth_sizes', bandwidth_sizes_default, @(x) (isnumeric(x) & (length(x) == 2) & (sum(x > 0) == 2)));
    class_plot_style_default = ['-r' '-b']; addOptional(p, 'class_plot_style', class_plot_style_default);
    show_legend_default = true; addOptional(p, 'show_legend', show_legend_default, @(x) (islogical(x)));
    class_names_default = ['X' 'Y']; addOptional(p, 'class_names', class_names_default);
    x_axis_default = 'Projected Values'; addOptional(p, 'x_axis', x_axis_default, @(x) (ischar(x)));
    y_axis_default = 'Density'; addOptional(p, 'y_axis', y_axis_default, @(x) (ischar(x)));
    parse(p, varargin{:})
    param_settings = cell2struct(struct2cell(p.Results), fieldnames(p.Results),1); % End of input parsing.
    
    X_projected = X_samples * beta;
    Y_projected = Y_samples * beta;
    quad_pts = sort([X_projected ; Y_projected]);
    if sum(param_settings.bandwidth_sizes > 0) == 0
        X_kde = ksdensity(X_projected, quad_pts);
        Y_kde = ksdensity(Y_projected, quad_pts);
    else
        X_kde = ksdensity(X_projected, quad_pts, 'bandwidth',param_settings.bandwidth_sizes(1));
        Y_kde = ksdensity(Y_projected, quad_pts, 'bandwidth',param_settings.bandwidth_sizes(2));
    end
    plot(quad_pts,X_kde, param_settings.class_plot_style(1), quad_pts, Y_kde, param_settings.class_plot_style(2))
    if param_settings.show_legend
        legend(param_settings.class_names(1), param_settings.class_names(2))
    end
    xlabel(param_settings.x_axis); ylabel(param_settings.y_axis);
end

