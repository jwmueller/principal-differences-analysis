% Example usage of the Matlab functions in the SPARDA package,
% which implement the Principal Differences Analysis methodology.
% Author: Jonas Mueller

% Each function takes in required arguments and some 
% additional optional arguments, which should be given as 
% 'argument_name', argument_value pairs.



%% First generate toy 10-dimensional Gaussian dataset in which first & last dimension of E[X] is different than E[Y]:
n = 150; m = 100; d = 10; 
seed = 12345; rng(seed) % set seed to control randomness.
X_samples = mvnrnd([1 zeros(1,d-2) 1], eye(d), n);
Y_samples = mvnrnd(zeros(1,d), eye(d), m);


%% Fast SPARDA analysis: 
lambdas = [0,1e-4,1e-3,1e-2,0.1:0.1:1,1.5,2,5,10]; % regularization penalties to try, set =0 for no regularization (ie. PDA instead of SPARDA).
[beta_hat, wass_dist, cost, best_lambda, param_settings] = fastSPARDA(X_samples, Y_samples, 'lambdas',lambdas,'num_folds',5, 'max_iter',1e4,'eps',1e-8,'learning_rate',0.1,'print_update',30);

%% Visualize projected X,Y distributions along estimated beta:
plotProjectedDistributions(X_samples, Y_samples, beta_hat, 'class_plot_style', ['-r','-b'], 'class_names', ['X' 'Y'])


%% SPARDA via Semidefinite Relaxation + subsequent tightening:
lambdas = [0, 0.01, 0.1, 1]; % regularization penalties to try, set =0 for no regularization (ie. PDA instead of SPARDA).
[beta_hat, wass_dist, cost, best_lambda, param_settings] = convexSPARDA(X_samples, Y_samples, 'lambdas',lambdas,'num_folds',2, 'max_iter',1e4, 'convergence_iter',300, 'eps',1e-8,'learning_rate',0.05,'print_update',100);


%% Permutation test for general distribution inequality between X and Y using fastSPARDA to find test statistic 
% For more rigorous results, one should use a finer grid of lambdas, 
% and also use convexSPARDA rather than fastSPARDA.
num_perm = 100;
lambdas = [0, 0.01, 0.1, 1];

permuted_statistics = zeros(1, num_perm);
[beta_hat, actual_dist, cost, best_lambda, param_settings] = fastSPARDA(X_samples, Y_samples, 'lambdas',lambdas,'num_folds',5, 'max_iter',1e4,'eps',1e-8,'learning_rate',0.1,'print_update',500);
for p=1:num_perm
    disp(['running permutation #',num2str(p),'...'])
    permutation_indices = randsample(n+m,n+m)';
    x_inds = permutation_indices(1:n);
    y_inds = permutation_indices((n+1):(n+m));
    X_permuted = [X_samples(x_inds(x_inds <= n),:); Y_samples(x_inds(x_inds > n)-n,:)];
    Y_permuted = [X_samples(y_inds(y_inds <= n),:); Y_samples(y_inds(y_inds > n)-n,:)];
    [permuted_beta, permuted_dist] = fastSPARDA(X_permuted, Y_permuted, 'lambdas',lambdas,'num_folds',5, 'max_iter',1e4,'eps',1e-8,'learning_rate',0.1,'print_update',Inf);
    permuted_statistics(p) = permuted_dist;
end
p_value = (sum(permuted_statistics >= actual_dist) + 1) / (num_perm + 1) 



%% Employ a simple random search for best projection:
[beta_randomsearch, wass_dist, param_settings] = randomProjectionSearch(X_samples, Y_samples, 'max_iter',100);
