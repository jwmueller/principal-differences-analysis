function [ beta, cost ] = cardinalityConstrainedSPARDA(xs, ys, k, max_iter, eps, convergence_iter, gamma, print_update, starting_truncation, beta0)
% Performs optimization of beta with respect to the original underlying
% nonconvex problem under hard cardinality constraint.

    if size(xs,1) < size(ys,1) % ensure n >= m:
       temp = xs;
       xs = ys;
       ys = temp;
    end
    n = size(xs, 1);
    m = size(ys, 1);
    d = size(xs, 2);
    if size(ys,2) ~= d
        error('X and Y do not have the same dimension');
    end
    
    iter = 0; best_iter = 0;
    if ~exist('beta0', 'var')
        beta0 = repmat(sqrt(d)/d,d,1) / 2;
    end

    beta = beta0;
    last_cost = -Inf; best_cost = -Inf;
    cost = projectedWasserstein(xs, ys, beta);
    grad = zeros(d,1);
    step_size = gamma;
    while (iter < max_iter) && (abs(cost - last_cost) >= eps)  && (iter - best_iter <= convergence_iter)
        last_cost = cost;
        iter = iter + 1;
        if any([1 2] == mod(iter, print_update))
            disp(['iter: ' num2str(iter)  '   cost: ' num2str(cost)  '  grad-norm: ' num2str(norm(grad)) '    beta_norm: ' num2str(norm(beta)) '  Step-size: ' num2str(step_size)])
        end
        
        % get gradient:
        [projected_xs, x_order] = sort(xs * beta);
        [projected_ys, y_order] = sort(ys * beta);
        grad = zeros(d,1);
        quant_x = 0;
        quant_y = 0;
        last_quant = 0;
        NUMERIC_FACTOR = 1e6; % used in ceil function to get the index of the quantile in the projected data vector.
        delta = 1/(n*m*NUMERIC_FACTOR); % small numerical factor used to convert floats to integer indices.
        x_index = 1; y_index = 1;
        while (quant_x < 1 - delta) || (quant_y < 1 - delta)
           next_quant_x = quant_x + 1/n;
           next_quant_y = quant_y + 1/m;
           proj_x = projected_xs(x_index);
           proj_y = projected_ys(y_index);
           while (next_quant_x < next_quant_y - delta)
               % disp('non-identical sample size, x-increment:'); disp([quant_x quant_y]); disp([x_index y_index]); disp(dist);
               grad = grad + 2 *(proj_x - proj_y) * (xs(x_order(x_index),:) - ys(y_order(y_index),:))' * (next_quant_x-last_quant);
               quant_x = next_quant_x;
               last_quant = quant_x;
               next_quant_x = quant_x + 1/n;
               x_index = min(x_index + 1, n);
               proj_x = projected_xs(x_index);
           end
           if (quant_x < 1 - delta) || (quant_y < 1 - delta)
               if (abs(next_quant_x - next_quant_y) < delta)
                   grad = grad + 2 *(proj_x - proj_y) * (xs(x_order(x_index),:) - ys(y_order(y_index),:))' * (next_quant_x-last_quant);
                   quant_x = next_quant_x;
                   quant_y = next_quant_y;
                   x_index = x_index + 1; y_index = y_index + 1;
                   last_quant = quant_x;
               else
                   grad = grad + 2 *(proj_x - proj_y) * (xs(x_order(x_index),:) - ys(y_order(y_index),:))' * (next_quant_x-last_quant);
                   quant_y = next_quant_y;
                   y_index = min(y_index + 1, m);
                   last_quant = quant_y;
               end
           end
        end % End of gradient computation.
        % disp(grad);
        
        step_size = gamma * max(1,(iter - convergence_iter/3))^(-1/2);
        beta = beta + step_size * grad;
        
        % truncation step (i.e. at most k nonzero entries, hard-contraint).
        if iter >= starting_truncation
            [~,ordering] = sort(abs(beta),'descend');
            beta(ordering((k+1):end)) = 0; % truncate.
            % disp(beta);
        end
        
        beta_norm = norm(beta);
        if beta_norm > 1
            beta = beta / norm(beta);
        end
        if beta(1) < 0
            beta = -beta;
        end
        
        cost = projectedWasserstein(xs, ys, beta);
        if (cost > best_cost) && (iter >= starting_truncation)
            best_iter = iter;
            best_cost = cost;
            best_beta = beta;
        end
    end
    beta = best_beta; cost = best_cost;
    if iter >= max_iter
        warning('tightening failed to converge')
    end
end

