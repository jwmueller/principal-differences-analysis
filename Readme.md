Matlab code for the paper: 

J. Mueller and T. Jaakkola. Principal Differences Analysis: Interpretable
Characterization of Differences between Distributions. NIPS 2015

Link to paper:  http://papers.nips.cc/paper/5894-principal-differences-analysis-interpretable-characterization-of-differences-between-distributions

A simple toy example showing the expected data-format and basic usage of our methods is given in:  example.m