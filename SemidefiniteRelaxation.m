function [beta, beta_cost, wass_dist, B_mat, B_cost, B_eigenvalues] = SemidefiniteRelaxation(xs, ys, lambda, max_iter, convergence_iter, eps, learning_rate, print_update, beta0)
    % Performs subgradient ascent in dual of semidefinite convex relaxation
    % to find SPARDA estimator. Subsequent tightening of relaxed solution is
    % recommended.
    % As a subgradient method, this approach may be sensitive to learning rate.
    % Returned values:
        % B_mat = Matrix solution for the semidefinite relaxation problem.
        % beta_cost = The relaxation cost of the proposed feasible solution (for the original SPARDA problem).
        % wass_dist = The projected Wasserstein distance in direction beta.
        % B_mat = The matrix which solves the semidefinite relaxation.
        % B_cost = The relaxation cost of B_mat
        % B_eigenvalues = Eigenvalues of this matrix.  The closer the largest is to 1, the tighter the relaxation.
    
    % Constants which control the optimization.
    DUAL_EXTRA_LEARN_RATE = 50; % multiplicative factor which enlarges the learning rate for dual updates.
    LEARN_RATE_DEC_START = 2 * convergence_iter; % the number of iterations after which to begin decreasing learning rates.
    LEARN_RATE_DECAY = -1/5; % exponential decay of learning after LEARN_RATE_DEC_START iterations.
    
    n = size(xs, 1);
    m = size(ys, 1);
    d = size(xs, 2); 
    % Set up constraints for QP to project into space of tr=1 matrices:
    qp_Aeq = ones(1,d); qp_beq = 1; qp_lb = zeros(d,1); qp_ub = ones(d,1);
    qp_opts = optimoptions('quadprog','Algorithm','interior-point-convex','Display','off');    
    
    % Initialization:
    if ~exist('beta0', 'var')
        beta0 = repmat(sqrt(d)/d,d,1) / 2;
    end
    B_mat = beta0 * beta0';
    iter = 0; cost = 0; % since we set dual variables = 0 and B_mat is rank 1.
    best_cost = cost; best_B = B_mat; best_iter = 0; max_eigenval = 0;
    us = zeros(n,1); vs = zeros(m,1); % dual variables initialization.
        
    while (iter < max_iter) && (iter - best_iter <= convergence_iter)
        iter = iter + 1;
        if any([0 1] == mod(iter, print_update))
            disp(['relaxation iter: ' num2str(iter)  '   relaxation cost: ' num2str(cost) '    Max eigenvalue(B_mat): ' num2str(max_eigenval)])
        end
        % Compute sub-gradient and cost:
        cost = 0;
        dB = zeros(d,d); du = repmat(1/n,n,1); dv = repmat(1/m,m,1);
        for i=1:n
            Z_i = bsxfun(@minus,xs(i,:),ys);
            C_i = sum((Z_i * B_mat') .* Z_i, 2);
            temp = C_i - vs - us(i);
            cost = cost + sum(min(0,temp));
            indices = (temp < 0);
            if sum(indices) > 0
                dB = dB + (Z_i' * Z_i);
                dv(indices) = dv(indices) - 1/m;
                du(i) = du(i) - sum(indices)/m;
            end
        end
        cost = cost/min(n,m) + sum(us)/n + sum(vs)/m;
        cost = cost - lambda * sum(abs(B_mat(:))); % sparsity penalization.
        if cost > best_cost + eps
            best_iter = iter; best_cost = cost; best_B = B_mat;
        end
        
        if norm(dB) > 1 % Use subgradient clipping since B is constrained in unit ball.
                dB = dB / norm(dB(:));
        end
        us = us + DUAL_EXTRA_LEARN_RATE * learning_rate * (max(1,iter-LEARN_RATE_DEC_START)^LEARN_RATE_DECAY) * du;
        vs = vs + DUAL_EXTRA_LEARN_RATE * learning_rate * (max(1,iter-LEARN_RATE_DEC_START)^LEARN_RATE_DECAY) * dv;
        B_stepsize = learning_rate * (max(1,iter-LEARN_RATE_DEC_START)^LEARN_RATE_DECAY);
        B_mat = B_mat + B_stepsize * dB;
        
        % Projection back into feasible set of matrices with tr-1 +
        % semidefiniteness:
        [U, S, V] = svd(B_mat);
        eigvals = diag(S);
        max_eigenval = max(abs(eigvals));
        little_v = quadprog(2*eye(d),-2*eigvals,[],[],qp_Aeq,qp_beq,qp_lb,qp_ub,[],qp_opts);
        B_mat = U * diag(little_v) * V';
        % Soft-threshold:
        B_mat(abs(B_mat) < lambda*B_stepsize) = 0;
        B_mat(B_mat > lambda*B_stepsize) = B_mat(B_mat > lambda*B_stepsize) - lambda*B_stepsize;
        B_mat(B_mat < -lambda*B_stepsize) = B_mat(B_mat < -lambda*B_stepsize) + lambda*B_stepsize;
    end
    
    B_mat = best_B; B_cost = best_cost;
    % Get BETA as largest eigenvector:
    [vecs,vals] = eig(B_mat); 
    vals = abs(diag(vals)); % convert to vector.
    max_index = find(vals==max(vals));
    max_index = max_index(1);
    beta = vecs(:,max_index);
    if (beta(1) < 0)
        beta = -beta;
    end
    wass_dist = projectedWasserstein(xs,ys,beta);
    B_eigenvalues = vals;
    
    % Ensure that relaxation solution is actually better than feasible solution found:
    feasible_mat = beta * beta';
    beta_cost = 0;
    for i=1:n
        Z_i = bsxfun(@minus,xs(i,:),ys);
        C_i = sum((Z_i * feasible_mat') .* Z_i, 2) - vs - us(i);
        beta_cost = beta_cost + sum(min(0,C_i));
    end
        
    if false % TODO remove!
    for i=1:n
        for j = 1:m
            Z_ij = xs(i,:) - ys(j,:);
            C_ij = Z_ij * feasible_mat * Z_ij' - us(i) - vs(j);
            beta_cost = beta_cost + min(0,C_ij);
        end
    end
    end
    
    
    beta_cost = beta_cost/min(n,m) + sum(us)/n + sum(vs)/m;
    beta_cost = beta_cost - lambda * sum(abs(feasible_mat(:)));
    if (beta_cost > B_cost + eps)
        warning('semidefinite optimization did not reach optimum; change max_iter or learning_rate')
    elseif (iter >= max_iter)
        warning('semidefinite optimization failed to converge; change max_iter, eps, or learning_rate')
    end
end

