function [ best_beta, best_dist, param_settings] = randomProjectionSearch(X_samples, Y_samples, varargin)
    % Simple random search to find projection BETA which maximizes projected Wasserstein distance.
    % Optional arguments: max_iter = the number of random projections to try.
    
    if size(X_samples,2) ~= size(Y_samples,2)
        error('X and Y must have same dimension');
    end % Parse input arguments:
    p = inputParser; p.FunctionName = 'randomProjectionSearch';
    default_max_iter = 100; addOptional(p, 'max_iter', default_max_iter, @(x) (isnumeric(x) && x > 0))
    parse(p, varargin{:}) 
    param_settings = cell2struct(struct2cell(p.Results), fieldnames(p.Results),1); % End of input parsing.
    
    d = size(X_samples,2);
    best_dist = -Inf;
    best_beta = NaN;
    for i = 1:param_settings.max_iter
        beta = mvnrnd(zeros(1,d),eye(d))';
        beta = beta/norm(beta);
        if beta(1) < 0
            beta = - beta;
        end
        d_beta = projectedWasserstein(X_samples, Y_samples, beta);
        if d_beta > best_dist
            best_dist = d_beta;
            best_beta = beta;
        end
    end
    % Also check basis directions:
    for l = 1:d
        beta = zeros(d,1);
        beta(l,:) = 1;
        d_beta = projectedWasserstein(X_samples, Y_samples, beta);
        if d_beta > best_dist
            best_dist = d_beta;
            best_beta = beta;
        end
    end
end

