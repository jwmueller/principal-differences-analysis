function [ beta_hat, wass_dist, cost, best_lambda, param_settings] = convexSPARDA(X_samples, Y_samples, varargin)
    % This method first solves a convex semidefinite relaxation of the
    % SPARDA objective, and then subsequently tightens the relaxation solution
    % with respect to the original (possibly nonconvex) objective.
    
    % Optional arguments:
    % 'lambdas' : a vector of possible regularization penalties to search
        % over (the best one is selected by cross-validation). 
        % Default is 0, i.e. no sparsity regularization (PDA instead of SPARDA).
    % 'num_folds' : number of cross-validation folds to use in lambda selection.
    % 'max_iter' : maximum number of iterations in the optimization.
    % 'convergence_iter' : number of consecutive iterations without objective function improvement to diagnose convergence in the relaxation subgradient optimization (larger values are more likely to produce optimal results, but will increase runtime).
    % 'eps' : numerical accuracy of optimization.
    % 'learning_rate' : learning rate (aka. step-size) to use in the
        % optimization. Smaller settings may lead to longer runtimes but better solutions. 
    % 'print_update' : number of iterations at which to print an update. Set = Inf to silence all output.
    
    % Parse input arguments:
    p = inputParser; p.FunctionName = 'fastSPARDA';
    lambas_default = 0; addOptional(p, 'lambdas', lambas_default, @(x) (isnumeric(x)))
    num_folds_default = 5; addOptional(p, 'num_folds', num_folds_default, @(x) (isnumeric(x)))
    max_iter_default = 1e4; addOptional(p, 'max_iter', max_iter_default, @(x) (isnumeric(x) && x > 0))
    convergence_iter_default = 500; addOptional(p, 'convergence_iter', convergence_iter_default, @(x) (isnumeric(x) && x > 0))
    eps_default = 1e-8; addOptional(p, 'eps', eps_default, @(x) (isnumeric(x) && x > 0))
    learning_rate_default = 1; addOptional(p, 'learning_rate', learning_rate_default, @(x) (isnumeric(x) && x > 0))
    print_update_default = 100; addOptional(p, 'print_update', print_update_default, @(x) (isnumeric(x) && x > 0))
    parse(p, varargin{:})
    param_settings = cell2struct(struct2cell(p.Results), fieldnames(p.Results),1); % End of input parsing.
    
    if size(X_samples,1) < size(Y_samples,1) % ensure n >= m:
           temp = X_samples;
           X_samples = Y_samples;
           Y_samples = temp;
    end
    n = size(X_samples, 1);
    m = size(Y_samples, 1);
    d = size(X_samples, 2);
    if size(Y_samples,2) ~= d
        error('X and Y do not have the same dimension');
    end
    ordering = randsample(n,n); % shuffle data.
    X_samples = X_samples(ordering,:);
    ordering = randsample(m,m);
    Y_samples = Y_samples(ordering,:);
    
    x_foldsize = floor(n/param_settings.num_folds); y_foldsize = floor(m/param_settings.num_folds);
    best_heldout = 0;
    prev_beta = zeros(d,1); prev_beta(1) = 0.5; % the last beta found, which we use for initialization to get faster convergence in next lambda setting.
    if length(param_settings.lambdas) > 1 % Use `cross-validation' to choose best penalty.
        param_settings.lambdas = sort(param_settings.lambdas); % search in increasing order of regularization.
        lambda_scores = zeros(1,length(param_settings.lambdas));
        % Find initial unregularized projection:
        [first_beta, first_cost] = relaxAndTighten(X_samples, Y_samples, 0, param_settings.max_iter, param_settings.convergence_iter, param_settings.eps, param_settings.learning_rate, param_settings.print_update*20, prev_beta);   
        for fold = 1:param_settings.num_folds
            % Assemble cross-val train/test-set:
            x_foldindex = (fold-1) * x_foldsize + 1;
            y_foldindex = (fold-1) * y_foldsize + 1;
            if fold < param_settings.num_folds
                xs_test = X_samples(x_foldindex:(x_foldindex+x_foldsize-1),:);
                xs_train = X_samples([1:(x_foldindex-1) (x_foldindex+x_foldsize):n],:);
                ys_test = Y_samples(y_foldindex:(y_foldindex+y_foldsize-1),:);
                ys_train = Y_samples([1:(y_foldindex-1) (y_foldindex+y_foldsize):m],:);                
            else
                xs_test = X_samples(x_foldindex:n,:);
                xs_train = X_samples(1:(x_foldindex-1),:);
                ys_test = Y_samples(y_foldindex:m,:);
                ys_train = Y_samples(1:(y_foldindex-1),:);
            end
            prev_beta = first_beta;
            for l=1:length(param_settings.lambdas)
                lambda = param_settings.lambdas(l);
                [beta, cost] = relaxAndTighten(xs_train, ys_train, lambda, param_settings.max_iter, param_settings.convergence_iter, param_settings.eps, param_settings.learning_rate, param_settings.print_update*10, prev_beta);
                if norm(beta) > 0
                    prev_beta = beta; % use this beta as the initialization for the next fold.
                    beta = beta / norm(beta); % always re-scale to unit norm before evaluating held out dist.
                end
                heldout_wass = projectedWasserstein(xs_test, ys_test, beta);
                lambda_scores(l) = lambda_scores(l) + heldout_wass;
                cardinality = sum(abs(beta) > 0);
                if param_settings.print_update < Inf
                    disp(['lambda: ' num2str(lambda) '  fold: ' num2str(fold)])
                    disp(['training cost: ' num2str(cost)])
                    disp(['heldout cost: ' num2str(heldout_wass)])
                    disp(['projection cardinality:' num2str(cardinality)])
                end
                if cardinality < 2
                    break
                end
            end
        end
        lambda_scores = lambda_scores / param_settings.num_folds;
        best_indices = find(lambda_scores == max(lambda_scores));
        best_lambda = param_settings.lambdas(best_indices(1)); % the best lambda.
    elseif length(param_settings.lambdas) == 1 % there is only one lambda given, so no need for hyperparameter search.
        best_lambda = param_settings.lambdas;
        prev_beta = randomProjectionSearch(X_samples, Y_samples, 'max_iter',max(10,ceil(param_settings.max_iter / 10)));
    else
        error('lambdas not correctly formatted')
    end
    % Re-Run method on full dataset using the best/only lambda:
    [beta_hat, cost] = relaxAndTighten(X_samples, Y_samples, best_lambda, param_settings.max_iter, param_settings.convergence_iter, param_settings.eps, param_settings.learning_rate, param_settings.print_update, prev_beta);
    beta_hat = beta_hat / norm(beta_hat); % always re-scale to unit norm.
    wass_dist = projectedWasserstein(X_samples, Y_samples, beta_hat);
    if param_settings.print_update < Inf
        disp(['best lambda found: ' num2str(best_lambda)])
        disp(['projection cardinality: ' num2str(sum(sum(abs(beta_hat) > param_settings.eps)))])
        disp(['projected Wasserstein distance:' num2str(wass_dist)])
    end
end

function [beta, tightened_dist, relaxed_beta, relaxed_cost, relaxed_dist] = relaxAndTighten(xs, ys, lambda, max_iter, convergence_iter, eps, learning_rate, print_update, beta0)
% Helper function which solves semidefinite relaxation and then tightens the resulting solution.
    [relaxed_beta, relaxed_cost, relaxed_dist] = SemidefiniteRelaxation(xs, ys, lambda, max_iter, convergence_iter, eps, learning_rate, print_update, beta0);
    [beta, tightened_dist] = tightening(xs, ys, max_iter, eps, learning_rate/10, print_update, relaxed_beta);
end

